import {ADD_TODO,UPDATE_TODO} from "../actions";
import {TOGGLE_TODO} from "../actions";

let id =  1; /// seria id gerado pelo banco de dados

const  todoListReducer = (state = [], action) => {
    switch (action.type){
        case ADD_TODO:
        /// Fazer Ação Recomendada
            const newTodo = {
              id: id++,
              text: action.text,
                done: false
            };
            return [... state,newTodo]; /// conotação para o mesmo acrescentar mas um array no state
        // caso o usuário clicar como feito a ação o mesmo irá verificar id recebido da ação com o enviado original da action
        case UPDATE_TODO:
            return state.map(todo =>{
                if (todo.id === action.todo.id){
                    return action.todo;
                }
                return todo;
            });

        // caso o usuário clicar como feito a ação o mesmo irá verificar o id da ação e riscara o evento ou desmarca
        case TOGGLE_TODO:
            return state.map(todo =>{
               if(todo.id === action.todoId){
                   return {
                       ...todo,
                       done: !todo.done
                   }
               }
               return todo;
            });


        default:
            return state;
    }

};


export default todoListReducer;