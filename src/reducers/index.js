import {combineReducers} from 'redux';
import todoListReducer from './todoListReducer'
import editingTodoReducer from './editingTodoReducer'


/// combineReducers e paracombinar os arrays

const rootReducer = combineReducers({
    ///está pegando somente a chave criada todoListReducers e colocando em todos
    todos: todoListReducer,
    ///está pegando somente a chave a ser editada e colocando em editingTodo
    editingTodo: editingTodoReducer

});

export default rootReducer;

