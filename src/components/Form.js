import React from 'react';
import {connect} from 'react-redux' /// faz a conexção dos componentes criados.
import {StyleSheet, Button, View} from 'react-native';
import Input from './Input';

import {addTodo,updateTodo,setTodoText} from "../actions";

class Form extends React.Component {

    render() {
        const {text, id} = this.props.todo;
        return (

            <View style={styles.containerForm}>
                <View style={styles.containerInput}>
                    <Input
                        onChangeText={(text) => this.onChangeText(text)}
                        value={text}
                    />
                </View>
                <View style={styles.containerButon}>
                    <Button
                        onPress={() => {
                            this.onPress();
                        }}
                        title = {id ? "Salve" : "Add"}
                    />
                </View>
            </View>
        );
    }

    /*
    * Pega o texto do input e set em text
    * @params: string text
    * @sise: 02/08/2018
    * @author:Lucas Lobato
    * */
    onChangeText(text) {
        this.props.dispachSetTodoText(text); /// para editar o ação.
    }

    /*
    * função de ao clicar no botão salva o text em uma list
    * @sise: 02/08/2018
    * @author:Lucas Lobato
    * */

    onPress() {
        const {todo} = this.props;
        if (todo.id)
            return this.props.dispachUpdateTodo(todo);
        ///Modo simplificado sem o else
        const {text} = todo;
        this.props.dispachAddTodo(text);
    }

}



/*
* Metódo short do função mapDispachProps

  const mapDispachtoProps = dispach => {
     return {
         dispachAddTodo: text => dispach(addTodo(text))
     }
 };

*
* */

// const mapDispachtoProps = {
//     dispachAddTodo: addTodo
// } ;


const mapStateToProps= state =>{
    return{
      todo:state.editingTodo
  }

};


    /*
    * connect recebe duas funções mapSatateToPops,  mapDispachtoProps
    * */
export default connect(mapStateToProps,{
    dispachAddTodo:addTodo,
    dispachSetTodoText:setTodoText,
    dispachUpdateTodo:updateTodo,
})(Form);


const styles = StyleSheet.create({
    containerForm: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10
    },
    containerInput: {
        flex: 4

    },
    containerButon: {
        flex: 1
    }

});