
import React from 'react';
import {View,Text, StyleSheet,TouchableOpacity} from 'react-native';

const TodoListItem = ({todo,onPressTodo,onLongPressTodo}) => (

        <TouchableOpacity onPress={onPressTodo}
                           onLongPress={onLongPressTodo} >
            <View style={styles.line}>
                <Text style={[styles.lineText,
                            todo.done ? styles.lineThrough : null
                ]}> {`${todo.id} ${todo.text}`}</Text>
        </View>
        </TouchableOpacity>
);


const styles = StyleSheet.create({ /* cria uma const de sytle que pode ser acessada pela função */

    line: {
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: "#bbb",
        alignItems: 'center',
        flexDirection: "row"
    },

    lineText: {
        fontSize: 20,
        paddingLeft: 15,
        flex:7 /* divide espaços em uma view*/
    },
    lineThrough:{
        textDecorationLine: 'line-through',
        backgroundColor: 'red'

    },
    avatar:{
        aspectRatio: 1, /* comando não distocer a imagem*/
        flex: 1,

        marginLeft:15, /*arredonda a imagem e coloca uma margem de 15 para esquerda*/
        borderRadius: 50  /* coloca altura da imagem de acordo com altura da linha no caso height 60*/

    }

});

export default TodoListItem;
