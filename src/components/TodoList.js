import React from 'react';
import {View} from 'react-native';

import TodoListItem from './TodoListItem';
import {toggle_todo,setEditingTodo} from '../actions';

import {connect} from 'react-redux';

const TodoList =({ todos,dispachToggleTodo,dispachSetEditingTodo}) =>(
    <View>
        {todos.map(todo =>
            <TodoListItem Key={todo.id}
                          todo={todo}
                          onPressTodo={() => dispachToggleTodo(todo.id)}
                          onLongPressTodo={() => dispachSetEditingTodo(todo)}

            />)}
    </View>
);



const mapStateToProps = state =>{
    const {todos} = state;
    return {todos};
};

export default connect (
    mapStateToProps,
    {dispachToggleTodo:toggle_todo,
        dispachSetEditingTodo:setEditingTodo
    })
(TodoList);