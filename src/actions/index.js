//Action addiciona nova tarefa.
export const ADD_TODO = 'ADD_TODO';
 export const addTodo =  text => ({
    type: ADD_TODO,
    text
});

 //Action marca tarefa ja realizado.
 export const TOGGLE_TODO = 'TOGGLE_TODO';
 export const toggle_todo  = todoId => ({
     type: 'TOGGLE_TODO',
     todoId,
 });

 //Action set o tarefa ja cadastrado edição.
 export const SET_TODO_TEXT = 'SET_TODO_TEXT';
 export const setTodoText =  text => ({
     type: SET_TODO_TEXT,
     text
 });

//Action set o tarefa o toda a ação cadastrada.
export const SETE_EDITING_TODO = 'SETE_EDITING_TODO';
export const setEditingTodo =  todo => ({
    type: SETE_EDITING_TODO,
    todo
})

//Action set o tarefa ja cadastrado edição.
export const UPDATE_TODO = 'UPDATE_TODO';
export const updateTodo =  todo => ({
    type: UPDATE_TODO,
    todo
});