
import React from 'react';
import {createStore } from 'redux';
import devToolsEnhancer from 'remote-redux-devtools'; ///  faz importação da ferramenta de debugagem
import {Provider} from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';

import Form from './components/Form';
import TodoList from './components/TodoList';
import rootReducer from './reducers';

const store = createStore(rootReducer,devToolsEnhancer());

export default class TodoApp extends React.Component {
    render() {
        return (

            <Provider store ={store}>

            <View style={styles.container}>
               <Form/>
               <TodoList/>
            </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop:30

    },
});